package org.cahsieh.main;

import com.opencsv.CSVReader;
import org.cahsieh.method.BinaryGA;
import org.cahsieh.method.PSO;
import org.cahsieh.method.SVMTrain;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by CANCHU on 2016/1/12.
 */
public class Main {

    public static void main(String[] args) {

//        final String[] filename = {"iris", "seeds", "sonar", "vertebral_column_data_column_2C", "transfusion"/*, "pendigits", "wilt"*/};
//        for (int n = 0; n < filename.length; n++) {
//            final String path = "E:\\BIFinal\\" + filename[n] + ".csv";
//        final String path = "E:\\ED\\newdata\\isAttrData_Training&Testing\\isFromURO\\SVM\\edmod_numerical_days_normalData_is_fromURO_training.csv";
//        final String path = "E:\\ED\\newdata\\isAttrData_Training&Testing\\isNotFromURO\\SVM\\edmod_numerical_days_normalData_is_not_fromURO_training.csv";
        final String path = "E:\\ED\\newdata\\isAttrData_Training&Testing\\Mix\\SVM\\edmod_numerical_days_training_1.csv";
        ArrayList<Record> recordList = new ArrayList<>();

        try {
            CSVReader reader = new CSVReader(new FileReader(path));
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                recordList.add(new Record(nextLine));
            }
            reader.close();

            //normalization
            int AttrNum = recordList.get(0).getAttrLength();

            for (int i = 0; i < AttrNum; i++) {
                double Max = recordList.get(0).getAttributes(i);
                double Min = recordList.get(0).getAttributes(i);
                for (Record record : recordList) {
                    if (record.getAttributes(i) > Max) {
                        Max = record.getAttributes(i);
                    }
                    if (record.getAttributes(i) < Min) {
                        Min = record.getAttributes(i);
                    }
                }

//                System.out.printf("%f\t%f\t",Max,Min);

                double Denominator = Max - Min;
                for (int j = 0; j < recordList.size(); j++) {
                    double current = recordList.get(j).getAttributes(i);
                    recordList.get(j).setAttributes(i, ((current - Min) / Denominator));
                }
            }



            new SVMTrain(recordList);

//            BinaryGA binaryGA = new BinaryGA(recordList);
//            binaryGA.run("result");
//            binaryGA.outputParameter("result");

//                PSO pso = new PSO(recordList);
//                pso.run(filename[n]);
//                pso.outputParameter(filename[n]);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
//        }
    }
}
