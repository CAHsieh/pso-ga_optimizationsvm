package org.cahsieh.main;

/**
 * Created by CANCHU on 2016/1/12.
 */
public class Record {

    private int attrLength;
    private String targetClass;
    private double[] Attributes;
    private double targetClassDouble;

    public Record(String[] originDate) {
        attrLength = originDate.length - 1;
        Attributes = new double[attrLength];
        targetClass = originDate[0];
        for (int i = 1; i < originDate.length; i++) {
            Attributes[i - 1] = Double.valueOf(originDate[i]);
        }

        int ascii = targetClass.charAt(0);
        targetClassDouble = (double) ascii;
    }

    public double getTarget() {
        return targetClassDouble;
    }

    public double getSelectFeature(int i) {
        return Attributes[i];
    }

    public int getAttrLength() {
        return attrLength;
    }

    public boolean isPredict(double v) {
        return v == targetClassDouble;
    }

    public double getAttributes(int idx) {
        return Attributes[idx];
    }

    public void setAttributes(int idx, double attributes) {
        Attributes[idx] = attributes;
    }
}
