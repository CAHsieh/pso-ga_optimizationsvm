package org.cahsieh.method;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by CAHsieh on 2016/1/14.
 */
public class Particle {

//    private static final int C_LENGTH = 13;
//    private static final int GAMMA_LENGTH = 13;


    private int FeatureLength;
    private double[] v;

    private double c;
    private double gamma;
    private boolean[] selectFeature;
    private double[] selectFeatureNum;
    private double Accurancy;

    private boolean[] bestselectFeature;
    private double[] bestselectFeatureNum;
    private double bestAccurancy;
    private double bestc;
    private double bestgamma;

    public Particle(int attrNum) {

        FeatureLength = attrNum;
        selectFeature = new boolean[FeatureLength];
        selectFeatureNum = new double[FeatureLength];
        Random random = new Random();
        setRadomX(random.nextInt(FeatureLength));

        v = new double[FeatureLength + 2];
        for (int i = 0; i < FeatureLength + 2; i++) {
            v[i] = (double) random.nextInt(10000) * 0.0001;
        }

        c = (double) random.nextInt(10000) * 0.01 + (double) random.nextInt(30);
        bestc = c;
        gamma = (double) random.nextInt(10000) * 0.01 + (double) random.nextInt(30);
        bestgamma = gamma;
        Accurancy = 0;
        bestAccurancy = 0;
    }

    public Particle(Particle particle) {
        this.selectFeatureNum = particle.getBestSelectFeatureNum();
        FeatureLength = selectFeatureNum.length;
        selectFeature = new boolean[FeatureLength];

        for (int i = 0; i < FeatureLength; i++) {
            selectFeature[i] = (selectFeatureNum[i] > 0.5);
        }
        this.Accurancy = particle.getBestAccurancy();

        this.c = particle.getBestC();
        this.gamma = particle.getBestGamma();
    }

    private void setRadomX(int N) {
        while (N > 0) {
            Random random = new Random();
            int idx;
            do {
                idx = random.nextInt(FeatureLength);
            } while (selectFeature[idx]);
            selectFeature[idx] = true;
            selectFeatureNum[idx] = 1;
            N--;
        }
        bestselectFeature = Arrays.copyOf(selectFeature, FeatureLength);
        bestselectFeatureNum = Arrays.copyOf(selectFeatureNum, FeatureLength);
    }

    public void setAccurancy(double Accurancy) {
        this.Accurancy = Accurancy;
        if (bestAccurancy == 0) bestAccurancy = Accurancy;
    }

    public double[] getSelectFeatureNum() {
        return selectFeatureNum;
    }

    public double getAccurancy() {
        return Accurancy;
    }

    public double getBestAccurancy() {
        return bestAccurancy;
    }

    public double[] getV() {
        return v;
    }

    public double[] getBestSelectFeatureNum() {
        return bestselectFeatureNum;
    }

    public double getC() {
        return c;
    }

    public double getBestC() {
        return bestc;
    }

    public double getGamma() {
        return gamma;
    }

    public double getBestGamma() {
        return bestgamma;
    }

    public boolean[] getSelectFeature() {
        return Arrays.copyOf(selectFeature, FeatureLength);
    }

    public boolean[] getBestSelectFeature() {
        return Arrays.copyOf(bestselectFeature, FeatureLength);
    }

    public void newStep(double[] SelectFeatureNum, double[] v, double c, double gamma) {
        this.v = v;
        this.c = c;
        this.gamma = gamma;
        this.selectFeatureNum = SelectFeatureNum;
        FeatureLength = SelectFeatureNum.length;
        for (int i = 0; i < FeatureLength; i++) {
            selectFeature[i] = (SelectFeatureNum[i] > 0.5);
        }

    }

    public boolean compare(double Accurancy) {
        boolean isBetter = false;
        this.Accurancy = Accurancy;
        if (Accurancy > this.bestAccurancy) {
            isBetter = true;
            bestc = c;
            bestgamma = gamma;
            bestselectFeature = Arrays.copyOf(selectFeature, FeatureLength);
            bestselectFeatureNum = Arrays.copyOf(selectFeatureNum, FeatureLength);
            this.bestAccurancy = Accurancy;
        }
        return isBetter;
    }

}
