package org.cahsieh.method;

import com.opencsv.CSVWriter;
import libsvm.svm;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;
import org.cahsieh.main.Record;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by CANCHU on 2016/1/12.
 */
public class PSO {

    private static final int PARTICLECOUNT = 30;
    private static final int ITERATECOUNT = 300;

    private static final double c1 = 2;
    private static final double c2 = 2;
    private static final double w = 0.9;


    private ArrayList<Record> list;
    private Particle[] particles;
    private Particle bestPaticle;
    private svm_problem _problem;
    private svm_parameter _parameter;


    public PSO(ArrayList<Record> list) {

        _problem = new svm_problem();

        _problem.l = list.size();
        double[] targets = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            targets[i] = list.get(i).getTarget();
        }
        _problem.y = targets;

        initParemeter();

        this.list = list;
        particles = new Particle[PARTICLECOUNT];
        bestPaticle = new Particle(list.get(0).getAttrLength());
        for (int i = 0; i < PARTICLECOUNT; i++) {
            particles[i] = new Particle(list.get(0).getAttrLength());
            particles[i].setAccurancy(getfitness(particles[i]));
            if (particles[i].getBestAccurancy() > bestPaticle.getAccurancy()) {
                bestPaticle = new Particle(particles[i]);
            }
        }

    }

    private void initParemeter() {
        _parameter = new svm_parameter();
        _parameter.svm_type = svm_parameter.C_SVC;
        _parameter.kernel_type = svm_parameter.RBF;
        _parameter.degree = 3;
        _parameter.coef0 = 0;
        _parameter.nu = 0.5;
        _parameter.cache_size = 100;
        _parameter.eps = 1e-3;
        _parameter.p = 0.1;
        _parameter.shrinking = 1;
        _parameter.probability = 0;
        _parameter.nr_weight = 0;
        _parameter.weight_label = new int[0];
        _parameter.weight = new double[0];

        //set in PSO
        _parameter.gamma = 0;
        _parameter.C = 1;
    }

    public void run(String filename) {
        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter("E:\\BIFinal\\PSO\\" + filename + "_iteration.csv"), ',');
            String str = "iteration&c&gamma&selectFeature&Accurancy";
            Write(csvWriter, str, "&");
            for (int times = 1; times <= ITERATECOUNT; times++) {
                //newStep
                for (int i = 0; i < PARTICLECOUNT; i++) {
                    newStep(particles[i]);
                }
                //selectBestParticle

                for (int i = 0; i < PARTICLECOUNT; i++) {
                    double nAccurancy = getfitness(particles[i]);
                    if (particles[i].compare(nAccurancy)) {
                        if (particles[i].getBestAccurancy() > bestPaticle.getAccurancy()) {
                            bestPaticle = new Particle(particles[i]);
                        }
                    }
                }

                for (int j = 0; j < PARTICLECOUNT; j++) {
                    Particle particle = particles[j];
                    double c = particle.getBestC();
                    double gamma = particle.getBestGamma();
                    boolean[] selectFeature = particle.getBestSelectFeature();
                    str = String.valueOf(times) + "&" + String.valueOf(c) + "&" + String.valueOf(gamma) + "&";
                    for (int i = 0; i < selectFeature.length; i++) {
                        if (selectFeature[i])
                            str = str + String.valueOf(i + 1) + " ";
                    }
                    str = str + "&" + String.valueOf(particle.getBestAccurancy());
                    Write(csvWriter, str, "&");
                }
                str = "&";
                Write(csvWriter, str, "&");
            }
            csvWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void outputParameter(String string) {

        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter("E:\\BIFinal\\PSO\\" + string + "_result.csv"), ',');

            String str = "ParticleID&c&gamma&selectFeature&Accurancy";
            Write(csvWriter, str, "&");

            for (int j = 0; j < PARTICLECOUNT; j++) {
                Particle particle = particles[j];
                double c = particle.getBestC();
                double gamma = particle.getBestGamma();
                boolean[] selectFeature = particle.getBestSelectFeature();
                str = String.valueOf(j) + "&" + String.valueOf(c) + "&" + String.valueOf(gamma) + "&";
                for (int i = 0; i < selectFeature.length; i++) {
                    if (selectFeature[i])
                        str = str + String.valueOf(i + 1) + " ";
                }
                str = str + "&" + String.valueOf(particle.getBestAccurancy());
                Write(csvWriter, str, "&");
            }

            double c = bestPaticle.getC();
            double gamma = bestPaticle.getGamma();
            boolean[] selectFeature = bestPaticle.getSelectFeature();
            str = "AllBest&" + String.valueOf(c) + "&" + String.valueOf(gamma) + "&";
            for (int i = 0; i < selectFeature.length; i++) {
                if (selectFeature[i])
                    str = str + String.valueOf(i + 1) + " ";
            }
            str = str + "&" + String.valueOf(bestPaticle.getAccurancy());
            Write(csvWriter, str, "&");
            csvWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private void newStep(Particle particle) {


        Random random = new Random();
        double ran1 = (double) random.nextInt(10000) * 0.0001;
        double ran2 = (double) random.nextInt(10000) * 0.0001;

        double[] mSelectFeaturenum = particle.getSelectFeatureNum();
        double[] mbSelectFeaturenum = particle.getBestSelectFeatureNum();
        double[] mV = particle.getV();
        double[] gbSelectFeaturenum = bestPaticle.getSelectFeatureNum();
        double mC = particle.getC();
        double mbC = particle.getBestC();
        double gbC = bestPaticle.getC();
        double mGamma = particle.getGamma();
        double mbGamma = particle.getBestGamma();
        double gbGamma = bestPaticle.getGamma();

        double[] SelectFeaturet = new double[mSelectFeaturenum.length];
        double[] Vt = new double[mSelectFeaturenum.length + 2];
        for (int i = 0; i < mSelectFeaturenum.length; i++) {
            double vti = (w * mV[i + 2]) + (c1 * ran1 * (mbSelectFeaturenum[i] - mSelectFeaturenum[i])) + (c2 * ran2 * (gbSelectFeaturenum[i] - mSelectFeaturenum[i]));
            double xti = mSelectFeaturenum[i] + vti;
            SelectFeaturet[i] = xti;
            Vt[i + 2] = vti;
        }

        Vt[0] = (w * mV[0]) + (c1 * ran1 * (mbC - mC)) + (c2 * ran2 * (gbC - mC));
        mC = ((mC + Vt[0]) <= 0) ? 0.1 : (mC + Vt[0]);
        Vt[1] = (w * mV[1]) + (c1 * ran1 * (mbGamma - mGamma)) + (c2 * ran2 * (gbGamma - mGamma));
        mGamma = ((mGamma + Vt[1]) < 0) ?0:(mGamma + Vt[1]);

        particle.newStep(SelectFeaturet, Vt, mC, mGamma);
    }

    private double getfitness(Particle particle) {

        //_parameter set c, gamma
        _parameter.gamma = particle.getGamma();
        _parameter.C = particle.getC();

        //selectFeature to _problem
        boolean[] selectFeature = particle.getSelectFeature();
        int featureNum = 0;
        for (boolean select : selectFeature) {
            if (select) featureNum++;
        }
        if (featureNum == 0) return 0;
        svm_node[][] nodes = new svm_node[list.size()][featureNum];
        for (int i = 0; i < list.size(); i++) {

            ArrayList<svm_node> nodelist = new ArrayList<>();

            for (int j = 0; j < selectFeature.length; j++) {
                if (selectFeature[j]) {
                    svm_node node = new svm_node();
                    node.index = j + 1;
                    node.value = list.get(i).getSelectFeature(j);
                    nodelist.add(node);
                }
            }

            nodes[i] = nodelist.toArray(new svm_node[0]);
        }
        _problem.x = nodes;


        double Accurancy = 0;
        double[] target = new double[_problem.l];
        svm.svm_cross_validation(_problem, _parameter, 10, target);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isPredict(target[i]))
                Accurancy++;
        }
        return Accurancy / list.size();
    }

    private void Write(CSVWriter writer, String str, String sym) {
        String[] entries = str.split(sym);
        writer.writeNext(entries);
    }
}
