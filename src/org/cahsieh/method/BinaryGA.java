package org.cahsieh.method;

import com.opencsv.CSVWriter;
import org.cahsieh.main.Record;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import libsvm.*;

/**
 * Created by CANCHU on 2016/1/12.
 */
public class BinaryGA {

    private static final int LEAVECOUNT = 10;
    private static final int ITERATECOUNT = 1000;

    private ArrayList<Record> list;
    private Chromsome[] chromsomes;
    private svm_problem _problem;
    private svm_parameter _parameter;
//    CSVWriter csvWriter;

    public BinaryGA(ArrayList<Record> list) {
        this.list = list;
        chromsomes = new Chromsome[LEAVECOUNT];
        for (int i = 0; i < LEAVECOUNT; i++) {
            chromsomes[i] = new Chromsome(list.get(0).getAttrLength());
        }

        _problem = new svm_problem();

        _problem.l = list.size();
        double[] targets = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            targets[i] = list.get(i).getTarget();
        }
        _problem.y = targets;

        initParemeter();
    }

    private void initParemeter() {
        _parameter = new svm_parameter();
        _parameter.svm_type = svm_parameter.C_SVC;
        _parameter.kernel_type = svm_parameter.RBF;
        _parameter.degree = 3;
        _parameter.coef0 = 0;
        _parameter.nu = 0.5;
        _parameter.cache_size = 100;
        _parameter.eps = 1e-3;
        _parameter.p = 0.1;
        _parameter.shrinking = 1;
        _parameter.probability = 0;
        _parameter.nr_weight = 0;
        _parameter.weight_label = new int[0];
        _parameter.weight = new double[0];

        //set in GA
        _parameter.gamma = 0.010463;
        _parameter.C = 0.458747;
    }

    public void run(String filename) {
        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter("E:\\ED\\data\\SVM\\" + filename + "_iteration.csv"), ',');
            String str = "iteration&c&gamma&selectFeature&Accurancy";
            Write(csvWriter, str, "&");
            for (int times = 0; times < ITERATECOUNT; times++) {
                //mutation
                Chromsome[] mutationChromsomes = new Chromsome[LEAVECOUNT];
                for (int i = 0; i < LEAVECOUNT; i++) {
                    mutationChromsomes[i] = chromsomes[i].getMutation1(); //實做的Mutation有兩種.
                }
                //crossover
                Chromsome[] crossoverChromsomes = new Chromsome[LEAVECOUNT];
                for (int i = 0; i < LEAVECOUNT; i++) {
                    Random random = new Random();
                    int first = random.nextInt(LEAVECOUNT);
                    int second;
                    do {
                        second = random.nextInt(LEAVECOUNT);
                    } while (first == second);
                    crossoverChromsomes[i] = chromsomes[first].getCrossover(chromsomes[second]);
                }
                //selection
                for (Chromsome chromsome : chromsomes) {
                    chromsome.setAccurancy(getfitness(chromsome));
                }
                for (Chromsome chromsome : mutationChromsomes) {
                    chromsome.setAccurancy(getfitness(chromsome));
                }
                for (Chromsome chromsome : crossoverChromsomes) {
                    chromsome.setAccurancy(getfitness(chromsome));
                }
                selection(chromsomes, mutationChromsomes, crossoverChromsomes);
                for (int j = 0; j < 8; j++) {
                    Chromsome chromsome = chromsomes[j];
                    double c = chromsome.getCost();
                    double gamma = chromsome.getGamma();
                    boolean[] selectFeature = chromsome.getSelectFeature();
                    str = String.valueOf(times) + "&" + String.valueOf(c) + "&" + String.valueOf(gamma) + "&";
                    for (int i = 0; i < selectFeature.length; i++) {
                        if (selectFeature[i])
                            str = str + String.valueOf(i + 1) + " ";
                    }
                    str = str + "&" + String.valueOf(chromsome.getAccurancy());
                    Write(csvWriter, str, "&");
                }
                str = "&";
                Write(csvWriter, str, "&");
            }
            csvWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private void selection(Chromsome[] A, Chromsome[] B, Chromsome[] C) {
        int s = 0;
        Chromsome[] All = new Chromsome[A.length + B.length + C.length];
        for (Chromsome chromsome : A) {
            All[s++] = chromsome;
        }
        for (Chromsome chromsome : B) {
            All[s++] = chromsome;
        }
        for (Chromsome chromsome : C) {
            All[s++] = chromsome;
        }

        //sort
        for (int i = 0; i < s - 1; i++) {
            for (int j = i + 1; j < s; j++) {
                if (All[i].getAccurancy() < All[j].getAccurancy()) {
                    Chromsome temp = All[i];
                    All[i] = All[j];
                    All[j] = temp;
                }
            }
        }

        //choose leave
        leaveMethod(All, 2);


    }

    private void leaveMethod(Chromsome[] All, int num) {
        // num=1 -> leave top10. ; num=2 -> leave top8 and random leave another randomNum.
        switch (num) {
            case 1:
                //top10
                chromsomes = null;
                chromsomes = Arrays.copyOfRange(All, 0, 10);
                break;
            case 2:
                int randomNum = 2;
                boolean[] ck = new boolean[All.length];
                chromsomes = new Chromsome[LEAVECOUNT];
                for (int i = 0; i < LEAVECOUNT - randomNum; i++) {
                    chromsomes[i] = new Chromsome(All[i]);
                    ck[i] = true;
                }

                for (int i = LEAVECOUNT - randomNum; i < LEAVECOUNT; i++) {
                    Random random = new Random();
                    int tmp;
                    do {
                        tmp = random.nextInt(All.length - (LEAVECOUNT - randomNum)) + (LEAVECOUNT - randomNum);
                    } while (ck[tmp]);
                    ck[tmp] = true;
                    chromsomes[i] = new Chromsome(All[tmp]);
                }
                break;
        }
    }

    private double getfitness(Chromsome chromsome) {

        //_parameter set c, gamma
        _parameter.gamma = chromsome.getGamma();
        _parameter.C = chromsome.getCost();

        //selectFeature to _problem
        boolean[] selectFeature = chromsome.getSelectFeature();
        int featureNum = 0;
        for (boolean select : selectFeature) {
            if (select) featureNum++;
        }
        if (featureNum == 0) return 0;
        svm_node[][] nodes = new svm_node[list.size()][featureNum];
        for (int i = 0; i < list.size(); i++) {

            ArrayList<svm_node> nodelist = new ArrayList<>();

            for (int j = 0; j < selectFeature.length; j++) {
                if (selectFeature[j]) {
                    svm_node node = new svm_node();
                    node.index = j + 1;
                    node.value = list.get(i).getSelectFeature(j);
                    nodelist.add(node);
                }
            }

            nodes[i] = nodelist.toArray(new svm_node[0]);
        }
        _problem.x = nodes;


        double Accurancy = 0;
        double[] target = new double[_problem.l];
        svm.svm_cross_validation(_problem, _parameter, 10, target);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isPredict(target[i]))
                Accurancy++;
        }
        return Accurancy / list.size();
    }

    public void outputParameter(String string) {

        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter("E:\\ED\\data\\SVM\\" + string + "_Top8_result.csv"), ',');

            String str = "c&gamma&selectFeature&Accurancy";
            Write(csvWriter, str, "&");

            for (int j = 0; j < 8; j++) {
                Chromsome chromsome = chromsomes[j];
                str = "";
                double c = chromsome.getCost();
                double gamma = chromsome.getGamma();
                boolean[] selectFeature = chromsome.getSelectFeature();
                str = String.valueOf(c) + "&" + String.valueOf(gamma) + "&";
                for (int i = 0; i < selectFeature.length; i++) {
                    if (selectFeature[i])
                        str = str + String.valueOf(i + 1) + " ";
                }
                str = str + "&" + String.valueOf(chromsome.getAccurancy());

                Write(csvWriter, str, "&");
            }
            csvWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }


    }

    private void Write(CSVWriter writer, String str, String sym) {
        String[] entries = str.split(sym);
        writer.writeNext(entries);
    }

}
