package org.cahsieh.method;

import libsvm.*;
import org.cahsieh.main.Record;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CANCHU on 2016/5/9.
 */
public class SVMTrain {

    private ArrayList<Record> list;
    private svm_problem _problem;
    private svm_parameter _parameter;
//    private String model_path = "E:\\ED\\newdata\\isAttrData_Training&Testing\\isFromURO\\SVM\\svm_model";
//    private String model_path = "E:\\ED\\newdata\\isAttrData_Training&Testing\\isNotFromURO\\SVM\\svm_model";
    private String model_path = "E:\\ED\\newdata\\isAttrData_Training&Testing\\Mix\\SVM\\svm_model";

    public SVMTrain(ArrayList<Record> list) {
        this.list = list;
        _problem = new svm_problem();

        _problem.l = list.size();
        double[] targets = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            targets[i] = list.get(i).getTarget();
        }
        _problem.y = targets;

        initParemeter();
        train();
    }

    private void initParemeter() {
        _parameter = new svm_parameter();
        _parameter.svm_type = svm_parameter.C_SVC;
        _parameter.kernel_type = svm_parameter.RBF;
        _parameter.degree = 3;
        _parameter.coef0 = 0;
        _parameter.nu = 0.5;
        _parameter.cache_size = 100;
        _parameter.eps = 1e-3;
        _parameter.p = 0.1;
        _parameter.shrinking = 1;
        _parameter.probability = 0;
        _parameter.nr_weight = 0;
        _parameter.weight_label = new int[0];
        _parameter.weight = new double[0];

        //set in GA
        _parameter.gamma = 0.0;
        _parameter.C = 1;
    }

    private void train() {

        svm_node[][] nodes = new svm_node[list.size()][9];
        for (int i = 0; i < list.size(); i++) {

            ArrayList<svm_node> nodelist = new ArrayList<>();

            for (int j = 0; j < 8; j++) {
                svm_node node = new svm_node();
                node.index = j + 1;
                node.value = list.get(i).getSelectFeature(j);
                nodelist.add(node);
            }



            nodes[i] = nodelist.toArray(new svm_node[0]);
        }
        _problem.x = nodes;
        int s = 0;
        List<Double[]> accurancys = new ArrayList<>();
        for (int costExp = -3; costExp < 15; costExp++) {
            for (int gammaExp = -3; gammaExp < 15; gammaExp++) {

                _parameter.C = Math.pow(2, costExp);
                _parameter.gamma = Math.pow(2, gammaExp);

                svm_model _model = svm.svm_train(_problem, _parameter);
                try {
                    svm.svm_save_model(model_path + "_" + String.valueOf(s) + ".txt", _model);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                double Accurancy = 0;
                for (int i = 0; i < list.size(); i++) {
                    double result = svm.svm_predict(_model, nodes[i]);
                    if (list.get(i).isPredict(result)) {
                        Accurancy++;
                    }
                }
                System.out.println("===============================================================\n" + String.valueOf(s) + ": Accurancy: " + String.valueOf(Accurancy / list.size()));
                accurancys.add(new Double[]{_parameter.C, _parameter.gamma, Accurancy});
                s++;
            }
        }

        System.out.println("===============================================================\nNo.\tC\tGamma\tCorrect Counts");
        for (int i = 0; i < s; i++) {
            System.out.println(String.valueOf(i) + "\t" + String.valueOf(accurancys.get(i)[0]) + "\t" + String.valueOf(accurancys.get(i)[1]) + "\t" + String.valueOf(accurancys.get(i)[2]));
        }
    }

}
