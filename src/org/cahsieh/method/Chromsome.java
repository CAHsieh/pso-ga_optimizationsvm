package org.cahsieh.method;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by CANCHU on 2016/1/12.
 */
public class Chromsome {

    private boolean[] chromsome;
    private int chromsomeLength;

    private final int COST_LENGTH = 14;
    private final int GAMMA_LENGTH = 14;
    private int attrNum;

    private double Accurancy;

    public Chromsome(int attrNum) {
        this.attrNum = attrNum;
        chromsomeLength = COST_LENGTH + GAMMA_LENGTH + attrNum;
        chromsome = new boolean[chromsomeLength];
        Random random = new Random();
        setRadomChromsome(random.nextInt(chromsomeLength));
    }

    public Chromsome(Chromsome c) {
        chromsome = c.getChromsome();
        chromsomeLength = chromsome.length;
        attrNum = chromsomeLength / (COST_LENGTH + GAMMA_LENGTH);
        Accurancy = c.getAccurancy();
    }

    private Chromsome(Chromsome c1, Chromsome c2) {
        //use to get crossover
        boolean[] c1Bool = c1.getChromsome();
        boolean[] c2Bool = c2.getChromsome();
        chromsomeLength = c1Bool.length;
        chromsome = new boolean[chromsomeLength];
        attrNum = chromsomeLength / (COST_LENGTH + GAMMA_LENGTH);
        Random random = new Random();
        for (int i = 0; i < chromsomeLength / 2; i++) {
            int idx;
            do {
                idx = random.nextInt(chromsomeLength);
            } while (chromsome[idx]);
            chromsome[idx] = true;
        }

        for (int i = 0; i < chromsomeLength; i++) {
            chromsome[i] = chromsome[i] ? c1Bool[i] : c2Bool[i];
        }

    }

    public void setChromsome(boolean[] chromsome) {
        this.chromsome = chromsome;
    }

    public void setAccurancy(double Accurancy) {
        this.Accurancy = Accurancy;
    }

    private void setRadomChromsome(int N) {
        while (N > 0) {
            Random random = new Random();
            int idx;
            do {
                idx = random.nextInt(chromsomeLength);
            } while (chromsome[idx]);
            chromsome[idx] = true;
            N--;
        }
    }

    public boolean[] getChromsome() {
        return chromsome;
    }

    public double getAccurancy() {
        return Accurancy;
    }

    public double getCost() {
        double costs = 0;
        for (int i = 0; i < COST_LENGTH; i++) {
            if (chromsome[i])
                costs += Math.pow(2, i);
        }
        costs *= Math.pow(2, -7);
        return costs;
    }

    public double getGamma() {
        double gamma = 0;
        for (int i = COST_LENGTH; i < COST_LENGTH + GAMMA_LENGTH; i++) {
            if (chromsome[i])
                gamma += Math.pow(2, i - COST_LENGTH);
        }
        gamma *= Math.pow(2, -7);
        return gamma;
    }

    public boolean[] getSelectFeature() {
        return new boolean[]{true, true, true, true, true, true, true, true, true};//Arrays.copyOfRange(chromsome, COST_LENGTH + GAMMA_LENGTH, chromsomeLength);
    }

    public Chromsome getMutation1() {
        //隨機取一個bit做 1->0 & 0->1
        Chromsome newChromsome = new Chromsome(attrNum);
        boolean[] newchrom = Arrays.copyOf(chromsome, chromsomeLength);

        Random random = new Random();
//        int k = random.nextInt(3);
//        if (k == 0) {
//            for (int i = 0; i < COST_LENGTH; i++) {
//                newchrom[i] = !newchrom[i];
//            }
//        } else if (k == 1) {
//            for (int i = COST_LENGTH; i < COST_LENGTH + GAMMA_LENGTH; i++) {
//                newchrom[i] = !newchrom[i];
//            }
//        } else {
//            for (int i = COST_LENGTH + GAMMA_LENGTH; i < chromsomeLength; i++) {
//                newchrom[i] = !newchrom[i];
//            }
//        }

        int mutationPosition = random.nextInt(chromsomeLength);
        newchrom[mutationPosition] = !newchrom[mutationPosition];
        newChromsome.setChromsome(newchrom);

        return newChromsome;
    }

    public Chromsome getMutation2() {
        //cost, gamma, attrNum 各隨機取一個bit做 1->0 & 0->1
        Chromsome newChromsome = new Chromsome(attrNum);
        boolean[] newchrom = Arrays.copyOf(chromsome, chromsomeLength);

        Random random = new Random();
        int costRan = random.nextInt(COST_LENGTH);
        int gammaRan = random.nextInt(GAMMA_LENGTH);
        int attrRan = random.nextInt(attrNum);

        newchrom[costRan] = !newchrom[costRan];
        newchrom[COST_LENGTH + gammaRan] = !newchrom[COST_LENGTH + gammaRan];
        newchrom[COST_LENGTH + GAMMA_LENGTH + attrRan] = !newchrom[COST_LENGTH + GAMMA_LENGTH + attrRan];

        newChromsome.setChromsome(newchrom);
        return newChromsome;
    }

    public Chromsome getCrossover(Chromsome chromsome2) {
        return new Chromsome(this, chromsome2);
    }
}
